const gods = require('./gods')
/*
Q5. Faça um código que retorne um novo array apenas com o(s) deus(es) que seja da classe "Hunter", do panteão "Greek" e possua a role "Mid".
*/

const filterGod = gods.filter(god => god.class === "Hunter" && god.pantheon === "Greek" && god.roles.includes('Mid'));

console.log(filterGod);
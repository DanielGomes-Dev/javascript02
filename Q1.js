/*
 Q1. Imprima o nome e a quantidade de features de todos os deuses usando uma única linha de código. (em aula)
*/

const gods = require('./gods');

gods.forEach(god => console.log(god.name, god.features.length));



/*
Q4. Faça um código que retorne um novo array com o nome de cada deus e entre parênteses, a sua classe.

*/
const gods = require('./gods');

const newArray = gods.map(god => `${god.name} (${god.class})`);

console.log(newArray);
